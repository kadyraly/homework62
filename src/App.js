import React, { Component, Fragment } from 'react';

import { Switch, Route} from "react-router-dom";
import About from "./component/About/About";
import Home from "./component/Home/Home";
import Contact from "./component/Contact/Contact";
import Header from "./component/Header/Header";


class App extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <Switch>
                    <Route path="/"  exact component={Home} />
                    <Route path="/about" component={About} />
                    <Route path="/contact" component={Contact} />

                </Switch>
                <div className="footer"><p>Copyright © 2004 - 2018 Pluralsight LLC. All rights reserved</p></div>
            </Fragment>

        );
    }
}

export default App;

