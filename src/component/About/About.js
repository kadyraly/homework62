import React, {Component} from 'react';
import './About.css';
import {NavLink, Route} from "react-router-dom";

class About extends Component {



    render () {
        return (
            <div className="about">
                <h1>About</h1>
                <p>Our on-demand courses enable you to learn at your own pace—from the fundamentals
                    to the most cutting-edge technologies. Work smarter and keep pace with technology
                    with content from top industry experts.<NavLink activeClassName="selected" to="/about/more">read more</NavLink></p>
                <Route path='/about/more' render={() => {
                    return (
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet architecto atque autem consequatur
                            corporis cum cupiditate debitis error et explicabo harum inventore minus nemo nesciunt optio praesentium quisquam, vel?
                        </p>
                    )
                }} />
            </div>
        );
    }

}

export default About;