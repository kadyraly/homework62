import React, {Component} from 'react';

import './Contact.css';

class Contact extends Component {



    render () {
        return (
            <div className='contact'>
                <h1>Customer Support</h1>
                <h3>+1 (801) 784-9007</h3>
                <h3>www.pluralsight.com</h3>

            </div>
        );
    }

}

export default Contact;